# GitLab_Pipelines_24_Hour_Branch_Check

This is a basic gitlab ci job stage to check the age of the first commit on a branch and display a warning if its older than 
24 hours, the idea is to help the programmers get a reminder when a branch is running too old to be able to get into
a trunk based development flow

it used git commands to make it as independent as possible to gitlab and a .sh script to calculate the branch first commit age that could be reused anywhere

Comments are appreciated :)

