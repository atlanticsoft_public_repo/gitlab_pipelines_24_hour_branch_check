#!/bin/sh
       current_head=$(git rev-parse HEAD)
       last_merge=$(git rev-list --min-parents=2 --max-count=1 HEAD)
       first_commit=$(git log --pretty=format:"%h" $last_merge..$current_head |  tail -n 1)
       branch_date=$(git show -s --format=%cd --date=unix $first_commit)
       current_time=$(date +%s)
       time_difference=$(( $current_time - $branch_date )) 
       twenty_four_hours=$((60*60*24))
        if [ $time_difference -gt $twenty_four_hours ];then
            echo "Branch is older than 24 hours";
            exit 1
            else
            exit 0
            fi
          fi